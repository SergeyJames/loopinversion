#!/bin/bash

if [ "$#" -ne 4 ]; then
	echo -e "\e[91m[ERROR: $0] Illegal number of parameters!\e[0m"
	exit -1
fi

export BUILD_TYPE=$2

if [ $4 == "rebuild" ]; then

	if [ -d $1/build-${BUILD_TYPE} ]; then
		cd $1/build-${BUILD_TYPE}
		rm -rf *
	else
		mkdir $1/build-${BUILD_TYPE}
		cd $1/build-${BUILD_TYPE}
	fi

	cmake ../ \
	 -DLoopInversion_ROOT=$1 \
	 -DCMAKE_INSTALL_PREFIX=../install \
	 -DCMAKE_BUILD_TYPE=$BUILD_TYPE

else
	echo -e "\e[42m[Resuming] $2!\e[0m"
	cd $1/build-${BUILD_TYPE}
fi

cmake --build . --config $BUILD_TYPE --target install -j$3

