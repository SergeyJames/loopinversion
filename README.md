## gitlab   [![Build Status](https://gitlab.com/SergeyJames/loopinversion/badges/master/pipeline.svg)](https://gitlab.com/SergeyJames/loopinversion/)

## How to build bash v.1

```sh
$ git clone --recursive https://gitlab.com/SergeyJames/loopinversion.git
$ cd loopinversion/
$ ./run/rebuild.sh $(pwd) ${BUILD_TYPE} 4 rebuild
$ ./build/bin/unit_tests
```
## Available values for `BUILD_TYPE` Debug, Release and Fast
### Used Options below to control Optimization


|   Debug  |   Release   |     Fast    |
| :-------:| :---------: | :---------: |
|  -g -O0  |     -O3     |   -Ofast    |



## Running the test

```sh
$ ./bin/LoopInversion ../data/text.txt
```

## Results

|               OS              |                     CPU                    |   BUILD_TYPE  |
| :---------------------------: | :----------------------------------------: | :-----------: |
|  Linux debian 4.19.0-6-amd64  |  Intel(R) Core(TM) i9-9900K CPU @ 3.60GHz  |      Fast    |


```sh
$ ./bin/LoopInversion ../data/text.txt
NoLoopInversion avg: 19.7102 microseconds
        NoLoopInversion max: 57 microseconds
LoopInversion avg: 14.0776 microseconds
        LoopInversion max: 51 microseconds

Diff:28.5771%
```
