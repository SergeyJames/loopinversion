#include <assert.h>
#include <chrono>
#include <iostream>
#include <fstream>
#include <thread>
#include <time.h>
#include <vector>

#include <algorithms/algorithms.h>
#include <algorithms/numeric.h>
#include <algorithms/utilities/scopedtimer.hpp>


void noLoopInversion(char* ch) noexcept;

void loopInversion(std::string& s) noexcept;

bool readFromFile(const char* path, std::string& testString);


int main(int argc, char const *argv[]) {

	if (argc < 2) {
		std::cerr << "Illegal number of parameters! Give a valid path to text file!" << std::endl;
		return -1;
	}

	std::string testString;
	if (!readFromFile(argv[0], testString)) {
		return -1;
	}


	constexpr size_t iterations = 10000;
	float avgNoLoopInversion = .0f;
	float avgLoopInversion   = .0f;

	float maxNoLoopInversion = .0f;
	float maxLoopInversion   = .0f;

	using duration = std::chrono::microseconds;

	{
		std::vector<double> results(iterations, 0.0);
		for (size_t j = 0; j < iterations; j++) {
			std::string forNoLoopInversion = testString;
			wrp::ScopedTimer<duration> st;
			for (size_t i = 0, count = forNoLoopInversion.size(); i < count; ++i) {
				noLoopInversion(&forNoLoopInversion[i]);
			}
			results[j] = st.getCurDuration();
		}
		avgNoLoopInversion = wrp::average(results);
		std::cout << "NoLoopInversion avg: " << avgNoLoopInversion << " " <<  wrp::chronoDurationPrettyName<duration>() << std::endl;
		assert(!results.empty());
		maxNoLoopInversion = *std::max_element(results.cbegin(), results.cend());
		std::cout << "	NoLoopInversion max: " << maxNoLoopInversion << " " <<  wrp::chronoDurationPrettyName<duration>() << std::endl;
	}

	{
		std::vector<double> results(iterations, 0.0);
		for (size_t j = 0; j < iterations; j++) {
			std::string forLoopInversion = testString;
			wrp::ScopedTimer<duration> st;
			loopInversion(forLoopInversion);
			results[j] = st.getCurDuration();
		}
		avgLoopInversion = wrp::average(results);
		std::cout << "LoopInversion avg: " << avgLoopInversion << " " <<  wrp::chronoDurationPrettyName<duration>() << std::endl;
		assert(!results.empty());
		maxLoopInversion = *std::max_element(results.cbegin(), results.cend());
		std::cout << "	LoopInversion max: " << maxLoopInversion << " " <<  wrp::chronoDurationPrettyName<duration>() << std::endl;
	}

	std::cout << std::endl;

	const auto [min, max] = std::minmax({avgNoLoopInversion, avgLoopInversion});
	std::cout << "Diff:" << 100 - ((min * 100) / max) << "%" << std::endl;
}


void noLoopInversion(char* ch) noexcept {
	if (*ch == 'e') {
		*ch = 'E';
	}
}

void loopInversion(std::string& s) noexcept {
	for (size_t i = 0, count = s.size(); i < count; ++i) {
		if (s[i] == 'e') {
			s[i] = 'E';
		}
	}
}

bool readFromFile(const char* path, std::string& testString) {
	std::ifstream fin(path, std::ios::in | std::ios::binary);
	if (!fin.good()) {
		std::cerr <<
			"Filed to open `"<<
			path <<
			"`. Give a valid path to text file!" << std::endl;
		return false;
	}

	fin.seekg(0, std::ios::end);
	const size_t size = static_cast<size_t>(fin.tellg());
	testString.resize(size);
	fin.seekg(0, std::ios::beg);
	fin.read(&testString[0], size);
	return true;
}
